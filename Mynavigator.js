/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StatusBar, StyleSheet, Text, View } from 'react-native';
import Home from './components/Home/SecondScreen'
import Cart from './components/Cart/Cart'
import FoodDetail from './components/FoodDetail/SecondScreen'
import { StackNavigator, TabNavigator, ThemeProvider, uiTheme, Toolbar } from 'react-navigation'
const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
            <ThemeProvider uiTheme={uiTheme}>
                <Toolbar leftElement="menu" centerElement="Searchable" searchable={{ autoFocus: true, placeholder: 'Search', }} />
            </ThemeProvider>
      </View>
    );
  }
}

export default App;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
