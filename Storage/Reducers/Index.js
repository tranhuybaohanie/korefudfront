import {combineReducers} from 'redux';
import Hotel from './Hotel';
import BigCategory from './BigCategory';
import ProductCategory from './ProductCategory';
import Language from './Language';
import Cart from './Cart';

const appReducers = combineReducers({
    BigCategory,
    ProductCategory,
    Language,
    Cart
})

export default appReducers;
