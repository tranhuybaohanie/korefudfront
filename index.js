/** @format */
import React from 'react';
// import 'core-js/es6/symbol';
// import 'core-js/fn/symbol/iterator';
import {AppRegistry} from 'react-native';
import App from './App';
// import {name as appName} from './app.json';
import appReducers from './Storage/Reducers/Index';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
const store = createStore(
    appReducers,
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(thunk)

);

class AppOut extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
            
        );
    }
}
AppRegistry.registerComponent('korefud', () => AppOut);

