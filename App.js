
import React, { Component } from 'react';
import { Platform, StatusBar, StyleSheet, Text, View, Navigator } from 'react-native';
import Home from './components/Home/HomeFood'
import Cart from './components/Cart/Main'
import { StackNavigator, TabNavigator, ThemeProvider, uiTheme, Toolbar } from 'react-navigation'
import Login from './components/Login/LoginScreen';
import Register from './components/Register/Register';
import { Router, Scene, Actions, ActionConst } from 'react-native-router-flux';
import { firebaseApp, db } from './utlils/firebaseConfig';
import AppMain from './AppMain';
import FoodIDetail from './components/Home/FoodIDetail';
import CheckOut from './components/Cart/CheckOut';
import CategoryPage from './components/Home/CategoryPage';
import ls from 'react-native-local-storage';
import wait from 'wait.for-es6';
import idex from './indexMan';

// import 'firebase/firestore/dist/index.cjs';
import { item } from './Mock/dbscheme'
// import {play} from './utlils/sound'
// import realm from './Database/AllSchemas';
// import {insertNewUser,queryAllUser,updateUser} from './Database/AllSchemas'
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


// export default class App extends React.Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text>Open up App.js to start working on your app!</Text>
//         <Text>Changes you make will automatically reload.</Text>
//         <Text>Shake your phone to open the developer menu.</Text>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });





console.disableYellowBox = true;


function MystackNavigator() {

  var valiLogin;

  return StackNavigator({

    Login: { screen: Login },
    Register: { screen: Register },
    MyHome: { screen: Home },
    FoodIDetail: { screen: FoodIDetail },
    CheckOut: { screen: CheckOut },
    CategoryPage: { screen: CategoryPage },
    idex: {
      screen: idex,

      navigationOptions: { header: null },

    },
    AppMain: {
      screen: AppMain,
      navigationOptions: { header: null, gesturesEnabled: false, },
      transitionConfig: () => ({
        transitionSpec: {
          duration: 1000,
          easing: Easing.out(Easing.poly(4)),
          timing: Animated.timing,
        },
        screenInterpolator: sceneProps => {
          const { layout, position, scene } = sceneProps;
          const { index } = scene;

          const height = layout.initHeight;
          const translateY = position.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [height, 0, 0],
          });

          const opacity = position.interpolate({
            inputRange: [index - 1, index - 0.99, index],
            outputRange: [0, 1, 1],
          });

          return { opacity, transform: [{ translateY }] };
        },
      }),
    }
  },
    {
      initialRouteName: 'idex',
    });

}

export default MystackNavigator();
