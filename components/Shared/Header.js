import React, { Component } from 'react';
import {
  CheckBox,
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Animated,
  TextInput,
  Dimensions,
  Easing,
  StatusBar,
  Modal,
  TouchableWithoutFeedback
} from 'react-native';
import ls from 'react-native-local-storage';
import { Actions, ActionConst } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient'
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
const SIZE = 80;

class Header extends Component {

  constructor(props) {
    super(props);

    this.state = {
      Language: this.props.Lang[0]["lang"] ? this.props.Lang[0]["lang"] : "vn",
      isLoading: false,
      flag_left: new Animated.Value(-5),
      optionLang_left: new Animated.Value(-200),
      isModalVisible: false,
      showMenu: new Animated.Value(0),
      isshowMenuOpacity: new Animated.Value(1),
      searchKey: ""
    };


    this._onPress = this._onPress.bind(this);
    this.growAnimated = new Animated.Value(0);
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  componentWillReceiveProps(newprops) {
    if (newprops.searchKey) { this.setState({ searchKey: newprops.searchKey }) }
    if (newprops.screenProps) {
      if (this.state.showMenu._value == -40 && newprops.screenProps.show_menu) {

        Animated.timing(
          this.state.showMenu,
          {
            toValue: 0,
            duration: 500
          }
        ).start();
        Animated.timing(
          this.state.isshowMenuOpacity,
          {
            toValue: 1,
            duration: 500
          }
        ).start();
      } else if (this.state.showMenu._value == 0 && !newprops.screenProps.show_menu) {
        Animated.timing(
          this.state.showMenu,
          {
            toValue: -40,
            duration: 500
          }
        ).start();
        Animated.timing(
          this.state.isshowMenuOpacity,
          {
            toValue: 0,
            duration: 500
          }
        ).start();
      }
    }
    var previousLang = this.props.Lang[0]["lang"];
    var newLang = newprops.Lang[0]["lang"];

    if (this.state.Language != newLang) {
      this.setState({
        Language: newprops.Lang[0]["lang"],
      });

    }
  }
  componentDidMount() {
    Animated.timing(
      this.state.flag_left,
      {
        toValue: 20,
        duration: 3000
      }
    ).start();
  }
  //   static navigationOptions =({navigation})=>{
  //     let tabBarLabel='Cart';
  //     let tabBarIcon=()=>{
  //       <Image source={require('../Images/left-arrow.png')} style={{width:26, height:26,tintColor:'white'}}/>
  //     }
  //     return {tabBarLabel,tabBarIcon}
  //   }
  static navigationOptions = {
    activeTintColor: '#81B247',
    tabBarIcon: () => (

      <Image
        source={require('../Images/bell.png')}
        style={{
          width: 26, height: 26,
          tintColor: focused ? 'green' : 'gray',
        }}
      />
    )
  }
  _onPress() {
    if (this.state.isLoading) return;

    this.setState({ isLoading: true });

    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 300,
      easing: Easing.quad,
    }).start();

    setTimeout(() => {
      Actions.pop();
    }, 500);
  }
  hideOptionLang = () => {
    this.setState({ isModalVisible: false })
    Animated.timing(
      this.state.optionLang_left,
      {
        toValue: -200,
        duration: 1000
      }
    ).start();
  }
  showOptionLanguage = () => {

    this.setState({ isModalVisible: true })
    Animated.timing(
      this.state.optionLang_left
      , {
        toValue: 20,
        duration: 300,
        easing: Easing.quad,
      }).start();


  }
  navigateSearch = (text) => {
    if (!this.props.onSearchPage) {
      this.setState({ searchKey: text })
      this.props.screenProps.navigation.navigate("Search", { searchKey: text })
    } else {
      this.props.setSearchKey(text)
    }
  }
  setLang(lang) {
    var obLang = [{ lang: lang }]
    ls.save('Lang', lang).then(() => { })

    this.props.updateLang(obLang);
    this.setState({ isModalVisible: false })
    Animated.timing(
      this.state.optionLang_left,
      {
        toValue: -200,
        duration: 1000
      }
    ).start();
  }
  render() {
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, SIZE],
    });

    return (

      //  <Animated.View style={{marginTop:this.state.showMenu}}>
      <Animated.View >

        <View style={styles.container}>
          <LinearGradient
            colors={['#bb3d3e', '#e77586', '#e77586']}
            start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }}
            style={{ height: 62, width: widthDevice, position: "absolute" }}
          ></LinearGradient>


          <StatusBar barStyle='light-content'></StatusBar>
          <TouchableOpacity activeOpacity={0.3} onPress={this.showOptionLanguage}>
            <Animated.Image source={this.state.Language == "vi" ? require("./../Images/flag-vn.png") : require("./../Images/flag-usa.png")} style={{ width: 27, height: 27, marginLeft: this.state.flag_left, marginTop: 15, opacity: this.state.isshowMenuOpacity }} />
          </TouchableOpacity>


          {/* <Animated.View style={{opacity:this.state.isshowMenuOpacity}}> */}
          <View style={{ flexDirection: "row" }}>
            <View style={styles.boxsearch}>
              <Image source={require('./../Images/searchicon.png')} style={styles.searchicon}></Image>



              <TextInput
                style={styles.textinput}
                autoCorrect={false}
                autoCapitalize={false}
                value={this.state.searchKey}
                placeholder={this.stran("Type here to search!")}
                onChangeText={(text) => { this.navigateSearch(text) }}
              />
              {this.state.searchKey != "" ? <TouchableOpacity onPress={() => { this.setState({ searchKey: "" }); this.navigateSearch("") }}><View style={{ backgroundColor: "gray", borderRadius: 20, width: 20, height: 20, justifyContent: "center", alignItems: "center" }}><Text style={{ color: "black" }}>x</Text></View></TouchableOpacity> : null}
            </View>
            <TouchableOpacity activeOpacity={0.3} >
              <Text style={{ marginLeft: 10, fontSize: 15, paddingLeft: 15,paddingTop:20, color: "white", fontWeight: "bold" }}>한</Text>
            </TouchableOpacity>
          </View>
          <TouchableWithoutFeedback onPress={this.hideOptionLang} >
            <View style={{ flex: 1, top: 100, position: "absolute", height: heightDevice, width: widthDevice, display: this.state.isModalVisible ? "flex" : "none" }}>
            </View>
          </TouchableWithoutFeedback>

          <Animated.View style={[styles.optionLanguage, { marginLeft: this.state.optionLang_left }]}>
            <LinearGradient
              colors={['#bb3d3e', '#e77586', '#e77586']}
              start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }}
              style={{ width: 190, height: 100, position: "absolute" }}
            ></LinearGradient>
            <View>


              <TouchableOpacity onPress={() => { this.setLang("vi") }} >
                <View style={styles.textOptionLanguage}><Image style={{ width: 25, height: 25, marginRight: 10 }} source={this.state.Language == "vi" ? require("./../Images/icon-select.png") : null}></Image><Text style={{ fontSize: 20, color: "white" }}>Việt Nam</Text></View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { this.setLang("en") }}>
                <View style={styles.textOptionLanguage}><Image style={{ width: 25, height: 25, marginRight: 10 }} source={this.state.Language == "en" ? require("./../Images/icon-select.png") : null}></Image><Text style={{ fontSize: 20, color: "white" }}>English</Text></View>
              </TouchableOpacity>
            </View>
          </Animated.View>
        </View>
      </Animated.View>



    );
  }
}
const widthDevice = Dimensions.get('window').width;
const heightDevice = Dimensions.get('window').height;
const styles = StyleSheet.create({

  container: {
    height: 60,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'red'
    // #45B8AC'

  },
  textOptionLanguage: {
    marginTop: 10,
    flexDirection: "row",
    fontWeight: "bold",
    color: "white"
  },
  textinput: {
    color: 'black',
    width: 250,
    borderRadius: 50,
    backgroundColor: 'white',
  },
  optionLanguage: {
    position: 'absolute',
    width: 190,
    height: 100,
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center",

    top: 70,
    zIndex: 1000,
    borderRadius: 12,
    // backgroundColor: '#009b77e0',
    shadowOpacity: 0.75,
    // shadowRadius: 5,
    shadowColor: 'black',
    shadowOffset: { height: 0, width: 0 },
  },

  boxsearch: {
    // position: 'absolute',
    top: 7,
    left: 10,
    marginTop: 2,
    width: 310,
    height: 35,
    backgroundColor: 'white',
    zIndex: 100,
    borderColor: 'black',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

    bottom: 5
  },
  searchicon: {
    width: 30,
    height: 30,


  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    borderRadius: 100,
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  circle: {
    height: SIZE,
    width: SIZE,
    marginTop: -SIZE,
    borderRadius: 100,
    backgroundColor: '#F035E0',
  },
  image: {
    width: 24,
    height: 24,
  },
});
const mapStateToProps = state => {
  return {
    Lang: state.Language,

  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    updateLang: (Lang) => {

      dispatch(actionRedux.updateLanguage(Lang))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);