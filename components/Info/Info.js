import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
  Dimensions,
  ScrollView
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import ls from 'react-native-local-storage';
import * as actionRedux from './../../Storage/Actions/Index';
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import Header from '../Shared/Header'


export default class Info extends Component {

  constructor(props) {
    super(props);


    this.state = {
      isLoading: false,
      item: {}
    };


  }
  componentDidMount() {
    ls.get('email').then((email) => {
      actionRedux.getAllUserCustomerCondition(email, (id, item) => {
        this.setState({ item })
      })


    });

  }

  _onPressExplore= () => {
   
    this.props.navigation.navigate('Home');

  }


  _onPress = () => {
    ls.save('email', "")
      .then(() => {
        ls.get('email').then((data) => {



        });
        // output should be "get: Kobe Bryant"
      })

    this.props.navigation.navigate('Login');

  }

  render() {

    var { item } = this.state;
    return (
      <View> <Header screenProps={{ navigate: this.props.navigation.navigate, navigation: this.props.navigation }}></Header>

        <ScrollView style={{height:HDevice}}>


          <View style={{
            flex: 1,
            height:HDevice,
            backgroundColor: "white",
            alignContent: 'center',

          }}>
            <Image style={{ width: WDevice, height: 200 }} source={item.user_profile == "" ? require("./../Images/avatar_default.jpg") : { uri: item.user_profile }} />
            <View style={{ width: WDevice, height: 100, marginBottom: -45, position: "relative", top: -55, flexDirection: "row", justifyContent: "center" }} >
              <View style={{ width: 110, height: 100, borderRadius: 50, borderWidth: 5, borderColor: "white" }}>
                <Image style={{ width: 100, height: 100, borderRadius: 50, borderWidth: 2, borderColor: "green" }} source={item.user_profile == "" ? require("./../Images/avatar_default.jpg") : { uri: item.user_profile }} />
              </View>
            </View>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={{ textAlign: "center", fontWeight: "bold", fontSize: 15 }}>{item.username ? item.username.toUpperCase() : ""}</Text>
              <Text style={{ textAlign: "center", fontWeight: "bold" }}>({item.email})</Text>
             {item.address?<View style={{margin:10,padding:10, width: WDevice-100, borderRadius: 20, borderWidth: 2, borderColor: "gray"}}>
             <Text style={{ textAlign: "center", fontWeight: "100" }}>{item.address.phone}</Text>
             <Text style={{ textAlign: "center", fontWeight: "100" }}>{item.address.fullname}</Text>
             <Text style={{ textAlign: "center", fontWeight: "100" }}>{item.address.address}</Text>
             </View>:null}
           <View style={{flexDirection:"row"}}>   
             <TouchableOpacity
                onPress={this._onPressExplore}
                style={styles.buttonExplore}
                activeOpacity={0.4}>
                <Text style={{ color: 'white' }}>Explore food</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this._onPress}
                style={styles.button}
                activeOpacity={0.4}>
                <Text style={{ color: 'white' }}>Logout</Text>
              </TouchableOpacity>
              </View>
            </View>

          </View>


        </ScrollView>
      </View>
    );
  }
}
const HeightBTN = 50;
const WDevice = Dimensions.get('window').width;
const HDevice = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 2,
    alignItems: 'center',
    backgroundColor: "white",
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 140,
    margin:10,
    height: HeightBTN,
    borderRadius: 7,
    zIndex: 99,
    backgroundColor: '#eca183',
    color: 'white',
    fontSize: 15,
    marginBottom: 5
  },
  buttonExplore: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 140,
    margin:10,
    height: HeightBTN,
    borderRadius: 7,
    zIndex: 99,
    backgroundColor: 'pink',
    color: 'white',
    fontSize: 15,
    marginBottom: 5
  },
  circle: {
    height: HeightBTN,
    width: HeightBTN,
    marginTop: -HeightBTN,
    borderRadius: 100,
    backgroundColor: '#F035E0',
  },
  image: {
    width: 24,
    height: 24,
  },
});
