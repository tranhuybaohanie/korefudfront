import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    TouchableWithoutFeedback,
    Vibration,

} from 'react-native';
import Svg, {
    Circle,
    Ellipse,
    G,
    LinearGradient,
    RadialGradient,
    Line,
    Path,
    Polygon,
    Polyline,
    Rect,
    Symbol,
    Text as TextSvg,
    Use,
    Defs,
    Stop
} from 'react-native-svg';
import VibrationReact from 'react-native-vibration'
import Sound from 'react-native-sound'
import { Actions, ActionConst } from 'react-native-router-flux';
import ls from 'react-native-local-storage'
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from '../../Storage/Actions/Index';
import { stranl } from '../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import { sound } from './../../utlils/sound';
const SIZE = 80;
class FoodItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Language: "en",
            item: this.props.item,
            sliderSession: 0,
            email: this.props.email,
            Cart: this.props.Cart[0].itemList,
            loveTemp: false,
            loveHeardOpa: new Animated.Value(0),
            loveHeardSize: new Animated.Value(0)
        }
    }

    btnAddCart = (itemID) => {
        Vibration.vibrate(50);
        let sound = new Sound('finish.mp3', Sound.MAIN_BUNDLE, (error) => {
            if (error) { } else {
                sound.setVolume(0.5);
                sound.play((success) => {
                    if (success) {
                    } else {
                        sound.reset();
                    }
                });
            }
        });

        var Cart = this.state.Cart;
        if (!Cart.some(e => e.id === itemID)) {
            var itemCart = { id: itemID, quantity: 1 }
            Cart.push(itemCart)
            ls.save('Cart', Cart).then(() => { })
            this.props.UpdateCart([{
                itemList: Cart
            }])
        }
    }
    componentDidMount() {

        const item = this.state.item
        console.log(this.state.email)
        if (item.love && item.love.includes(this.state.email)) {

            if (!this.state.loveTemp)
                this.setState({ loveTemp: true })
        } else {
            if (this.state.loveTemp)
                this.setState({ loveTemp: false })
        }
    }
    componentWillReceiveProps(newprops) {

        if (newprops.Cart[0].itemList) {
            this.setState({
                Cart: newprops.Cart[0].itemList
            })
        }
        if (newprops.item) {

            this.setState({ item: newprops.item }, () => {
                const item = this.state.item
                if (item.love && item.love.includes(this.state.email)) {
                    if (!this.state.loveTemp)
                        this.setState({ loveTemp: true })
                } else {
                    if (this.state.loveTemp)
                        this.setState({ loveTemp: false })
                }
            })
        }

        var previousLang = this.props.Lang[0]["lang"];
        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });

        }

    }
    Love = (id) => {
        if (!this.state.loveTemp) {
            sound("likesound.mp3", 1)
            Animated.sequence([Animated.timing(
                this.state.loveHeardSize,
                {
                    toValue: 200,
                    duration: 800
                }
            ),

            Animated.timing(
                this.state.loveHeardSize,
                {
                    toValue: 0,
                    duration: 100
                }
            ),
            Animated.timing(
                this.state.loveHeardOpa,
                {
                    toValue: 0,
                    duration: 50
                }
            )

            ]).start();

            Animated.timing(
                this.state.loveHeardOpa,
                {
                    toValue: 1,
                    duration: 100
                }
            ).start();
        }
        this.setState({ loveTemp: !this.state.loveTemp }, () => {
            actionRedux.Love(id, this.state.loveTemp, result => {
            })
        })
    }
    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }

    stran = (key) => {
        return stranl(this.state.Language, key);
    }
    showDetail = (item) => {
        this.props.navigate("FoodIDetail", { item, email: this.state.email })
    }

    render() {

        var lang = this.state.Language;

        var { item } = this.state;

        function dotPrice(price) {
            var result = ""
            var final=""
             for (var i = price.length - 1; i >= 0; i--) {
                result += price[i]
            }
            for (var i =0; i < result.length; i++) {
                if (i % 3 == 0 && i != 0) {
               
                	//if(i>=0){
                    final += "."
                    //}
                }
                final += result[i]
            }
          	result=""
            for (var i = final.length - 1; i >= 0; i--) {
                result += final[i]
            }
            return result   
        }

        return (


            <View key={this.props.key} style={styles.container}>
                <TouchableWithoutFeedback onPress={() => { this.showDetail(item) }} >
                    <View style={styles.content}>
                        <Image source={{ url: item.img_url }} style={{ flex: 1, height: 200 }}></Image>
                        <Text style={styles.detailcontentback}> </Text>
                        {item.promotion_price > 0 ? <View style={{ height: 20, width: 50, top: 5, position: "absolute", right: 0 }}>
                            <Svg
                                height="20"
                                width="50"

                            >
                                <Text style={{ color: "red", paddingLeft: 10 }}>{Math.round(((item.promotion_price / item.price) * 100))}%</Text>
                                <Defs>
                                    <LinearGradient id="grad" x1="0" y1="0" x2="180" y2="0">
                                        <Stop offset="0" stopColor="yellow" stopOpacity="0.8" />

                                    </LinearGradient>
                                </Defs>

                                <Polygon
                                    points="0,0 50,0 50,20 -5,50 5,10 "
                                    fill="url(#grad)"

                                />

                            </Svg>
                        </View> : null}
                        <View style={{ position: "absolute", zIndex: 100, justifyContent: "center", flexDirection: "row", alignItems: "center", height: 200, width: WDevice }}>
                            <Animated.Image source={require('./../Images/heart_love.png')} style={{ width: this.state.loveHeardSize, height: this.state.loveHeardSize, opacity: this.state.loveHeardOpa }}></Animated.Image>
                        </View>
                        <View style={styles.detailcontent}>
                            <Text style={styles.namefood}>{eval("item.name_" + lang)}</Text>

                            <Text style={styles.description}>{this.limitString(eval("item.description_" + lang), 40)}</Text>
                            {/* <Text  style={styles.readmore}>Read more{"\n"}</Text> */}


                            {item.promotion_price && item.promotion_price > 0 ? <Text style={styles.price}>{dotPrice(item.promotion_price)} {item.currency}{"\n"}</Text> : null}
                            {item.promotion_price && item.promotion_price > 0 ? <Text style={styles.pricepromotion}>{dotPrice(item.price)} {item.currency}</Text> : null}
                            {!item.promotion_price || item.promotion_price < 1 ? <Text style={styles.price}>{dotPrice(item.price)} {item.currency}</Text> : null}



                        </View>

                    </View>
                </TouchableWithoutFeedback>

                {item.love && item.love.length > 0 ? <View style={{ flexDirection: "row", justifyContent: "flex-end", padding: 3 }}> <Text style={{ paddingTop: 7 }}>{item.love ? item.love.length : 0} </Text> <Image source={require('../Images/love-emoticon.gif')} style={{ width: 30, height: 30, marginBottom: 5, tintColor: "red" }}></Image></View> : null}
                <View style={styles.comboreview}>
                    <View style={styles.comboitem} >
                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }} onPress={() => { this.Love(item.id) }}>
                            {this.state.loveTemp ?
                                <View style={{ flexDirection: "row" }}>
                                    <Image source={require('../Images/love-emoticon.png')} style={{ width: 20, height: 20 }}></Image>
                                    <Text style={{ marginLeft: 3, color: "red" }}>Love</Text>
                                </View>
                                :
                                <View style={{ flexDirection: "row" }}>
                                    <Image source={require('../Images/hearticon.png')} style={{ width: 20, height: 20 }}></Image>
                                    <Text style={{ marginLeft: 3 }}>Love</Text>
                                </View>}
                        </TouchableOpacity>
                    </View>
                    <View style={styles.comboitem} >
                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }} onPress={() => { this.showDetail(item) }}>
                            <Image source={require('../Images/commenticon.png')} style={{ width: 18, height: 18 }}></Image>
                            <Text style={{ marginLeft: 3 }}>Comment</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.comboitem} >
                        {!this.state.Cart.some(e => e.id === item.id) ? <TouchableOpacity onPress={() => this.btnAddCart(item.id)} style={{ flexDirection: "row", alignItems: "center" }}>
                            <Image source={require('../Images/addcart.png')} style={{ width: 20, height: 20 }}></Image>
                            <Text style={{ marginLeft: 3 }}>Cart</Text>
                        </TouchableOpacity>
                            : <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image source={require('../Images/icon-added.png')} style={{ width: 20, height: 20 }}></Image>
                                <Text style={{ marginLeft: 3 }}>Cart added</Text>
                            </TouchableOpacity>}
                    </View>
                </View>


            </View>

        )




    }
}


const WDevice = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        //backgroundColor: 'white',

        marginTop: 5,

    },
    content: {
        flex: 1,
        flexDirection: "column",
        //  backgroundColor: 'none',

        marginTop: 3,
        height: 200
    },
    detailcontent: {
        marginLeft: 3,
        position: 'absolute',
        left: 0,
        top: 100,
        backgroundColor: "#e0cdcd00",
        height: 105
    },
    detailcontentback: {
        position: 'absolute',
        left: 0,
        top: 100,
        width: Dimensions.get('window').width,
        height: 100,
        backgroundColor: "#79747487",
    }, namefood: {
        fontSize: 22,
        fontWeight: "bold",
        color: "white"
    }, price: {
        marginTop: 5,
        fontSize: 18,
        fontWeight: "bold",
        color: "yellow",
        flex: 1

    }, pricepromotion: {
        marginBottom: 5,
        fontSize: 15,
        fontWeight: "bold",
        color: "pink",
        textDecorationLine: "line-through",


    }, description: {
        color: "white"
    },
    readmore: {
        fontWeight: "bold"
    },
    comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center",
        backgroundColor: "white",
        marginTop: 10,
        borderRadius: 20,
    },
    comboitem: {
        borderRadius: 20,
        height: 40,
        flex: 0.33,
        marginTop: 3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})





const mapStateToProps = state => {
    return {
        Lang: state.Language,
        Cart: state.Cart,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        },
        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodItem);