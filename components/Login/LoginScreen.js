import React, {Component} from 'react';
// import PropTypes from 'prop-types';
import Logo from './Logo';
import Form from './Form';
import Wallpaper from './Wallpaper';
import ButtonSubmit from './ButtonSubmit';
import SignupSection from './SignupSection';
import { firebaseApp } from '../../utlils/firebaseConfig';
import { Actions } from 'react-native-router-flux';
import ls from 'react-native-local-storage';
import {View,Image} from 'react-native'
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';




export default class LoginScreen extends Component {
  constructor(props){
    super(props);
    this.state={
      userName:"",
      passWord:"",
      isLogin:false
    }

    
  }


  static navigationOptions = {    header: null}

  login = () => {
    
    actionRedux.checkLogin(this.state.userName, this.state.passWord,(result)=>{
      if(result==true){
        this.props.navigation.navigate('Home');
      }else{
        alert("Please, check username or password again!!")
      }
    })
    // firebaseApp.auth().signInWithEmailAndPassword(this.state.userName, this.state.passWord)
    //   .then((data) => {
    //     // alert("Hi!  you create successful your account.");
    //     // window.localStorage.setItem('emailForSignIn', this.state.userName);
    //     // alert(window.localStorage.getItem('emailForSignIn'));
    //    // Actions.AppMain();
    //     //  Actions.Login();
    //     ls.save('email', data.user.email)
    //       .then(() => {
    //       })


    //     // ls.save('password', password)
    //     //   .then(() => {
    //     //     ls.get('password').then((data) => { console.log("get: ", data) });
    //     //     // output should be "get: Kobe Bryant"
    //     //   })
       

       
        
    //   })
    //   .catch(function (error) {
    //     // Handle Errors here.
    //     var errorCode = error.code;
    //     var errorMessage = error.message;
    //     alert(errorMessage);
    //     // ...
    //   });
  }


  onChangeState = (key, value) => {
    
    if (key == "userName" && this.state.userName != value) {
      this.setState({
        [key]: value
      });
    }
    if (key == "passWord" && this.state.passWord != value) {
      this.setState({
        [key]: value
      });
      //  alert(this.state.passWord); 

    }


  }
 
   render() {
 
   if(this.state.isLogin){
     return<View>
       
     </View>;
   }else{
   
    return (
      
      <Wallpaper>
        <Logo />
        <Form onChangeState={this.onChangeState}/>
        <SignupSection navigation={this.props.navigation}/>
        
        <ButtonSubmit login={this.login} />
      </Wallpaper>
    );
  }
 
  }
}
