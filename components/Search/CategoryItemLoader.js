import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions
} from 'react-native';

import { Actions, ActionConst } from 'react-native-router-flux';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';


const SIZE = 80;
class FoodItem extends Component {
 constructor(props) {
     super(props);
        this.state = {
         backgroundColor: new Animated.Value(0)
        }
 }
    componentDidMount(){
setInterval(()=>{
    var value=this.state.backgroundColor._value==0?1:0;
Animated.timing(this.state.backgroundColor,
        {
            toValue:value,
            duration:1900,
            //  easing: Easing.quad,
    // useNativeDriver: true
        }).start()
},2000)
        
    }

    render() {
var backgroundColor = this.state.backgroundColor.interpolate({
    inputRange: [0, 1],
    outputRange: ['#50525229', 'white']
});

        return (
            <View >
              
                  <View style={styles.container}>
                      <Animated.Image style={{ flex: 1, height: 200, borderRadius: 10,backgroundColor:backgroundColor}} />

                      
                      <Animated.Image style={{ flex: 1, position: 'absolute', top: 45, left: 8, height: 60, width: 60, borderRadius: 10,backgroundColor:backgroundColor }}  />
                      <View style={styles.detailcontent}>
                      <Animated.Text style={{...styles.detailcontentTextTitle,backgroundColor:backgroundColor,height: 10, width: 200 }}></Animated.Text>
                      <Animated.Text style={{...styles.detailcontentTextDescription,backgroundColor:backgroundColor,height: 20,marginTop:10, width: 220, }}></Animated.Text>
                      </View>

                  </View>
        
            </View>  
        )
    }
}


const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: 'white',
         height: 110, 
         width: WDevice-16, 
         marginLeft: 8,
        marginRight: 4,
         borderRadius:10,
         marginBottom:8
        //backgroundColor: 'white',

        

    },
    content: {
        flex: 1,
        flexDirection: "column",
        marginTop: 3,
        height: 200
    },
    detailcontent: {
        marginLeft:4,
        position: 'absolute',
        left: 80,
        top: 60,
        fontWeight:'bold',
       // backgroundColor: "#e0cdcd00",
        height: 105,
        color:'white',
    },
     detailcontentTextTitle: {
       
        fontWeight:'bold',
        fontSize:18,
        color:'white',
    },
     detailcontentTextDescription: {
       
        fontWeight:'bold',
        fontSize:14,
        color:'white',
    },
    detailcontentback: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: Dimensions.get('window').width-16,
        height: 110,
        backgroundColor: "#79747487",
        borderRadius: 10
    },namefood:{
        fontSize:25,
        fontWeight:"bold",
        color:"white"
    },price:{
        marginLeft: 3,
        position: 'absolute',
        left: 0,
        top: 75,
        // backgroundColor: "#e0cdcd00",
        height: 105,
        fontWeight:"bold",
        color:"yellow",
        

    },pricepromotion:{
        marginLeft: 3,
        position: 'absolute',
        left: 0,
        top: 89,
        // backgroundColor: "#e0cdcd00",
        height: 105,
        fontWeight: "bold",
        color: "red",
        textDecorationLine: 'line-through'

    },description:{
        color:"white"
    },
     readmore:{
        fontWeight:"bold"
    },
    comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center"
    },
    comboitem: {
        height: 40,
        flex: 0.33,
        marginTop:3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})




const mapStateToProps = state => {
    return {
        Lang: state.Language,

    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodItem);
