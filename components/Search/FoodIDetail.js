import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    Vibration,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard,
    StatusBar
} from 'react-native';
import Swiper from 'react-native-swiper';
import Sound from 'react-native-sound'
import {sound} from './../../utlils/sound';
import { Actions, ActionConst } from 'react-native-router-flux';
import ls from 'react-native-local-storage'
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
const SIZE = 80;
class FoodIDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Language: this.props.Lang[0]["lang"],
            item: this.props.navigation.getParam('item'),
            imgMoreCount: 0,
            email: this.props.navigation.getParam('email'),
            Cart: this.props.Cart[0].itemList,
            comment:"",
            commentList:[],
            loveTemp:false,
        }
       
    }
    componentDidMount() {
    
        const { navigation: { setParams } } = this.props;
        setParams({
            title: this.state.Language == "en" ? "Detail" : "Chi tiết"
        });
        actionRedux.getProductByID(this.state.item.id, result => {
       
            this.setState({ item: result })
        })
        actionRedux.getCommentOnProduct(this.state.item.id, result => {
       
            this.setState({ commentList: result })
        })
        const item = this.state.item
        if(item.love&&item.love.includes(this.state.email)){
            if(!this.state.loveTemp)
            this.setState({loveTemp:true})
        }else{
            if(this.state.loveTemp)
            this.setState({loveTemp:false})
        }
    }
    componentWillReceiveProps(newprops) {

        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            alert(newLang)
            this.setState({
                Language: newLang,
            });
            const { navigation: { setParams } } = this.props;
                setParams({
                    title: newLang == "en" ? "Detail" : "Chi tiết"
                });
            
          
        }
    }
    setStateKey=(key,value)=>{
        if(eval("this.state."+key)!=value)
        this.setState({[key]:value})
    }
    btnAddCart = (itemID) => {
        Vibration.vibrate(50);
        let sound = new Sound('finish.mp3', Sound.MAIN_BUNDLE, (error) => {
            if (error) { } else {
                sound.setVolume(0.5);
                sound.play((success) => {
                    if (success) {
                    } else {
                        sound.reset();
                    }
                });
            }
        });

        var Cart = this.state.Cart;
        if (!Cart.some(e => e.id === itemID)) {
            var itemCart = { id: itemID, quantity: 1 }
            Cart.push(itemCart)
            ls.save('Cart', Cart).then(() => { })
            this.props.UpdateCart([{
                itemList: Cart
            }])
        }
    }

    static navigationOptions = ({ navigation }) => {


        return {
            title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'Food' : navigation.state.params.title,
            headerStyle: {
                backgroundColor: 'white',


            }, navigationOptions: {
                header: {
                    style: {
                        shadowOpacity: 0,
                        shadowOffset: {
                            height: 0,
                            width: 0
                        },
                        shadowRadius: 0,
                    }
                }
            }
        }
    }
    getImgMore = (imgMore) => {
        if (imgMore) {
            var imgMoreArr = imgMore.split("{bao&%/break}");
            if (this.state.imgMoreCount != imgMore.length)
                this.setState({ imgMoreCount: imgMore.length })
            return imgMoreArr.map((item, idex) => {

                return (
                    <Image source={{ uri: item }} style={{ width: WDevice, height: 246 }} />
                );

            });
        }
    }
    getCommentList=(commentList)=>{
       
       return commentList.map((item,idd)=>{
        var finalTime="";
        var  create_date =item.create_date;
        var time =create_date.split(",")[0]
        var date =create_date.split(",")[1].split("/")
        var  d = new Date().toLocaleString("vi");
        var dt =d.split(",")[0]
        var dd=d.split(",")[1].split("/")
        var setNewRead=false;
        if(dd[2]==date[2]&&dd[1]==date[1]){
        if(dd[0]==date[0]){
        finalTime=this.state.Language=="en"?"Today at "+time:"Hôm nay lúc "+time
        setNewRead=time.split(":")[0]==dt.split(":")[0]&&dt.split(":")[1]-time.split(":")[1]<15?true:false
            
        }else{
        finalTime=dd[0]-date[0]+ this.state.Language=="en"?" days ago at "+time:" ngày trước lúc "+time
        }
        }else{
        finalTime=create_date
        }
return(
    <View style={{flexDirection:"row",marginTop:10}}>
        <Image source={require('./../Images/avatar_default.jpg')} style={{width:50,height:50,borderRadius:30}}></Image>
   <View>
   <View style={{backgroundColor:"#eff1f3",borderRadius:10,marginTop:5,padding:3}}> 
      <Text style={{fontWeight:"bold"}}>{item.email.split("@")[0]}</Text>
          <Text style={{maxWidth:WDevice-90}}>{item.comment}</Text>
      
      </View>
      <Text style={{maxWidth:WDevice-90,fontSize:10}}>{finalTime}</Text>
      </View>
        
    </View>
)
        })
    }
    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }
    Love = (id) => {
        if(!this.state.loveTemp){sound("likesound.mp3",1)}
        this.setState({loveTemp:!this.state.loveTemp},()=>{
            actionRedux.Love(id,this.state.loveTemp,result=>{
            })
        })
    }
    sendComment=()=>{
        if(this.state.comment!=""){
        actionRedux.Comment(this.state.item.id,this.state.email,this.state.comment,result=>{
           if(result) this.setStateKey("comment","")
           Keyboard.dismiss()
        })}
    }
    stran = (key) => {
        return stranl(this.state.Language, key);
    }

    render() {
        var stranl = this.stran;
        var lang = this.state.Language;

        const item = this.state.item
    
        return (
            <View style={styles.container}>
       
        {/* <StatusBar  backgroundColor="blue"
     barStyle="light-content"
     ></StatusBar> */}
     <StatusBar barStyle = "dark-content" backgroundColor="blue" hidden = {false}/>
                <View style={styles.container_top_profile}>
                    <Image source={{ url: item.img_url }} style={{ width: 50, height: 50, borderRadius: 10, marginLeft: 5, marginRight: 5, marginTop: 5 }}></Image>
                    <View>
                        <Text style={styles.namefood}>{eval("item.name_" + lang)}</Text>
                        {item.promotion_price && item.promotion_price > 0 ? <Text style={styles.price}>{item.promotion_price} {item.currency}</Text> : null}
                        {item.promotion_price && item.promotion_price > 0 ? <Text style={styles.pricepromotion}>{item.price} {item.currency}</Text> : null}
                        {!item.promotion_price || item.promotion_price < 1 ? <Text style={styles.price}>{item.price} {item.currency}</Text> : null}
                    </View>
                </View>

                <ScrollView>
                    {item.img_more?<Swiper style={{ height: 200, marginBottom: 5 }} showsButtons={false} activeDotColor="white"
                        autoplay={true} loop={true} paginationStyle={styles.dot}
                        key={this.state.imgMoreCount.length}
                    >

                        {this.getImgMore(item.img_more)}
                    </Swiper>:null}

                    {item.love && item.love.length > 0 ? <View style={{ flexDirection: "row", justifyContent: "flex-end", padding: 3 }}> <Text>{item.love ? item.love.length : 0} </Text> <Image source={require('../Images/hearticon.png')} style={{ width: 15, height: 15, tintColor: "red" }}></Image></View> : null}
                    <View style={styles.comboreview}>
                        <View style={styles.comboitem} >
                            <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }} onPress={() => { this.Love(item.id) }}>
                                { this.state.loveTemp ?
                                    <View style={{ flexDirection: "row" }}>
                                        <Image source={require('../Images/hearticon.png')} style={{ width: 20, height: 20, tintColor: "red" }}></Image>
                                        <Text style={{ marginLeft: 3, color: "red" }}>Love</Text>
                                    </View>
                                    :
                                    <View style={{ flexDirection: "row" }}>
                                        <Image source={require('../Images/hearticon.png')} style={{ width: 20, height: 20 }}></Image>
                                        <Text style={{ marginLeft: 3 }}>Love</Text>
                                    </View> }
                            </TouchableOpacity>
                        </View>
                        <View style={styles.comboitem} >
                            <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }} >
                                <Image source={require('../Images/commenticon.png')} style={{ width: 18, height: 18 }}></Image>
                                <Text style={{ marginLeft: 3 }}>Comment</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.comboitem} >
                            {!this.state.Cart.some(e => e.id === item.id) ? <TouchableOpacity onPress={() => this.btnAddCart(item.id)} style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image source={require('../Images/addcart.png')} style={{ width: 20, height: 20 }}></Image>
                                <Text style={{ marginLeft: 3 }}>Cart</Text>
                            </TouchableOpacity>
                                : <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                                    <Image source={require('../Images/icon-added.png')} style={{ width: 20, height: 20 }}></Image>
                                    <Text style={{ marginLeft: 3 }}>Cart added</Text>
                                </TouchableOpacity>}
                        </View>
                    </View>
                    <View style={styles.content}>


                        <View style={styles.detailcontent}>



                            {/* <Text  style={styles.readmore}>Read more{"\n"}</Text> */}



                            <View style={{ borderWidth: 1, borderRadius: 4, borderColor: "gray", margin: 4, padding: 4 }}>
                                <Text style={styles.description}>{eval("item.description_" + lang)}</Text>
                            </View>
                            <Text style={styles.h2}>{stranl("Ingredient")}</Text>
                            <View>
                                <Text style={styles.description}>{eval("item.ingredient_" + lang)}</Text>
                            </View>
                            <Text style={styles.h2}>{stranl("Nutritional value")}</Text>
                            <View>
                                <Text style={styles.description}>{eval("item.nutritional_value_" + lang)}</Text>
                            </View>


                        </View>

                    </View>
                    <View>

                  <View style={{borderColor:"pink",borderWidth:2,borderRadius:5,padding:5}}>
                  <View style={styles.content}>
                        {this.getCommentList(this.state.commentList)}
                    </View>
                    </View>
                    </View>
                </ScrollView>
                <KeyboardAvoidingView  behavior="padding"  keyboardVerticalOffset={55} enabled style={{flexDirection:"row"}}>
        
                <View style={styles.inputWrapper}>
                      
                        <TextInput
                            style={styles.input}
                            placeholder={stranl("Write a comment ...")}
                            value={this.state.comment}
                            autoCorrect={true}
                            autoCapitalize={true}
                            returnKeyType={true}
                            placeholderTextColor="gray"
                            multiline={true}
                            numberOfLines={4}
                            underlineColorAndroid="transparent"
                             onChangeText={(text)=>{this.setStateKey("comment",text)}}
                        />
                       
                    <TouchableOpacity onPress={()=>this.sendComment()}>
                    <Image source={require('./../Images/icon-send.png')} style={styles.inlineImg} />
                    </TouchableOpacity>
                </View>
                </KeyboardAvoidingView>
            </View>
        )
    }
}


const WDevice = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: 'white',
        marginTop: 0,



    },
    container_top_profile: {
        borderColor:"white",
        borderWidth:2,
        borderRadius:5,
        padding:10,
        flexDirection: "row",
        backgroundColor: "white"
    },
    h2: {
        marginTop: 5,
        color: "black",
        fontSize: 17
    },
    content: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "flex-start",
        backgroundColor: "white",
        marginTop: 3,

        padding: 10,
        marginTop: 5,
    },
    detailcontent: {
        marginLeft: 3,

        backgroundColor: "#e0cdcd00",

    },
    namefood: {
        fontSize: 22,
        fontWeight: "300",
        color: "pink"
    }, dot: {

        // marginLeft: 290,
        // marginBottom: 250

    }
    , price: {

        fontSize: 18,
        fontWeight: "200",
        color: "gray"

    }, pricepromotion: {
        marginTop: 1,
        fontSize: 15,
        fontWeight: "100",
        color: "gray",
        textDecorationLine: "line-through",


    }, description: {
        color: "gray",
        fontSize: 17
    },
    readmore: {
        fontWeight: "bold"
    },
    comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center",
        backgroundColor: "white",
        marginTop: 10,
        marginLeft: 4,
        marginRight: 4,
        borderRadius: 20,
    },
    comboitem: {
        borderRadius: 20,
        height: 40,
        flex: 0.33,
        marginTop: 3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    },
    input: {
        backgroundColor: '#f7f4f4',
        width: WDevice - 70,
        minHeight: 25,
        marginHorizontal: 20,
         marginTop:5,
         marginBottom:5,
         paddingTop:10,
        paddingLeft: 10,
        borderRadius: 20,
        color: "black",
      },
      inputWrapper: {
       height:50,
       width: WDevice,
       flexDirection:"row",
    borderTopWidth:1,
    borderColor:"pink"
      },
      inlineImg: {
        // position: 'absolute',
        zIndex: 99,
        width: 26,
        height: 26,
       marginRight:5,
        top: 9,
      }
})





const mapStateToProps = state => {
    return {
        Lang: state.Language,
        Cart: state.Cart,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        },
        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodIDetail);