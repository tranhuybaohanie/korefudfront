import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
  FlatList,
  Button,
  SearchBar,
  Icon,
  ScrollView
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import { navigate } from 'react-navigation'
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import FoodList from '../../Mock/FoodList';
import FoodItem from './FoodItem'
import Header from '../Shared/Header';
import { GetAllFood } from '../../Networking/FetchApi'
import FoodListMock from '../../Mock/FoodList'
import SquareItem from './SquareItem'
import { StackNavigator, TabNavigator, ThemeProvider, uiTheme, Toolbar, NavigationActions } from 'react-navigation'
import { db } from '../../utlils/firebaseConfig';
import Slider from './Slider';
import CategoryItem from './CategoryItem';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
const SIZE = 80;
const test = (n) => { alert(n) }
class HomeDrink extends Component {

  constructor(props) {
    super(props);

    this.state = {
      Language: this.props.Lang[0]["lang"],
      isLoading: false,
      foodListFromServer: [],
      itemsMenu: [],
      itemsSlider: [],
      config_menu: {},

    };

    this._onPress = this._onPress.bind(this);
    this.growAnimated = new Animated.Value(0);
  }

  componentWillReceiveProps(newprops) {

    var previousLang = this.props.Lang[0]["lang"];
    var newLang = newprops.Lang[0]["lang"];
    if (this.state.Language != newLang) {
      this.setState({
        Language: newprops.Lang[0]["lang"],
      });

      const { navigation: { setParams } } = this.props;
      setParams({
        title: newprops.Lang[0]["lang"] == "en" ? "Dink" : "Thức uống"
      });
    }
  }
  componentWillMount() {
    const { navigation: { setParams } } = this.props;
    setParams({
      title: this.state.Language == "en" ? "Dink" : "Thức uống"
    });
  }
  componentDidMount() {


    // this.props.navigation.dispatch(setParamsActionKarte);
    // actionRedux.getMenuConfig((result) => {

    //   this.setState({
    //     itemsMenu: result.itemsMenu,
    //     itemsSlider: result.slider,
    //     config_menu: result
    //   })
    // })


  }
  refreshDataFromServer = () => {
    GetAllFood('hotel', 'GET', null).then((res) => {

    });

  }

  static navigationOptions = ({ navigation }) => {


    return {
      title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'find' : navigation.state.params.title,

      //  headerRight:<Button title="info"></Button>,
      header: { visible: true },
      //  headerTinColor:'red',
      //   activeTintColor:'blue',
      tabBarIcon: ({ focused }) => (

        <Image
          source={require('../Images/cart.png')}
          style={{
            width: 26, height: 26,
            tintColor: focused ? 'green' : 'gray',
          }}
        />

      )
    }
  }

  _onPress() {
    if (this.state.isLoading) return;

    this.setState({ isLoading: true });

    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 300,
      easing: Easing.quad,
    }).start();

    setTimeout(() => {
      Actions.pop();
    }, 500);
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  render() {
    // console.log(this.props)
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, SIZE],
    });
    var { config_menu } = this.state;
    // console.log(config_menu)
    const { navigate } = this.props.navigation;
    var lang = this.state.Language;

    return (

      <ScrollView style={styles.container}>

        {/* <Button title="Pushdetail"
          onPress={() => { navigate('FoodDetail') }}
        ></Button> */}
        {/* <View>
          <Slider itemsSlider={this.state.itemsSlider}></Slider>
        </View>




        <View style={styles.compoItem}>
          <Text style={{ fontSize: 19, flex: 0.05, textAlign: 'center', marginTop: 20 }}>{eval("config_menu.title_food_" + lang) ? eval("config_menu.title_food_" + lang) : "Welcome to Korefud"}</Text>
          <Text style={{ textAlign: 'center', color: 'gray', marginBottom: 20 }}>{eval("config_menu.caption_food_" + lang) ? eval("config_menu.caption_food_" + lang) : "Eat all these if you want! Drink all if you can!"}</Text>

          <View style={styles.compoItem}>
            <Text style={styles.textTitle}>Category </Text>

            <CategoryItem></CategoryItem>
            <CategoryItem></CategoryItem>
            <CategoryItem></CategoryItem>
          </View>
        </View>



        <View style={styles.compoItem}>

          <Text style={styles.textTitle}>NEWEST RECIPES ></Text>
          <ScrollView horizontal={true} >
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
          </ScrollView>
          <Text style={styles.textTitle}>TRENDING RECIPES ></Text>
          <ScrollView horizontal={true} >
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
          </ScrollView>
        </View>


        <View style={styles.compoItem}>
          <Text style={styles.textTitle}>TODAY ></Text>
          <FlatList
            data={this.state.itemsMenu}
            keyExtractor={(item, index) => item.key}
            renderItem={(item, index) => {


              return (<FoodItem key={index} item={item} />)
            }}
          ></FlatList>
        </View> */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,

    // backgroundColor: '#b5b7bd'
    //alignItems: 'flex-end',
    //justifyContent: 'flex-end',
  },
  textTitle: {
    marginLeft: 8,
    marginTop: 10,
    marginBottom: 7,
    fontSize: 17,
    fontWeight: 'bold'

  },
  compoItem: {
    backgroundColor: 'white',
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 40

  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    borderRadius: 100,
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  circle: {
    height: SIZE,
    width: SIZE,
    marginTop: -SIZE,
    borderRadius: 100,
    backgroundColor: '#F035E0',
  },
  image: {
    width: 24,
    height: 24,
  },
});

const mapStateToProps = state => {
  return {
    Lang: state.Language,

  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeDrink);