import React, { Component } from 'react';
import Search from './Search'

import { StackNavigator, TabNavigator } from 'react-navigation'
import Header from '../Shared/Header'
import {
    TabBarBottom,
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Button,
    SearchBar,
    Dimensions,
    Icon
} from 'react-native';
 


const WDevice = Dimensions.get('window').width;

const Main = TabNavigator({


    MainHome: {
    screen:(props)=>{ return( <Search screenProps= {{navigate:props.screenProps.navigate,hideMenu:props.screenProps.hideMenu,navigation:props.screenProps.navigation}}></Search>)},
        navigationOptions: {

            tabBarLabel: "Tab 1",

        }
    }
}, 
    {
        
        ...TabNavigator.Presets.AndroidTopTabs,

        tabBarComponent: (props, state) => {
           return( <View style={{zIndex:1000}}>
                {/* {...TabNavigator.Presets.AndroidTopTabs} */}
                {/* <Header screenProps={{show_menu:props.screenProps.show_menu}}></Header> */}
               
            </View>)
    }
    
        
    })

class MainNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = { show_menu: true};

    }
    // componentDidMount() {
    //     this.onLoadCats().then((cats) => this.setState({ cats: cats }));
    // }
    hideMenu=(req)=>{
        this.setState({
            show_menu:!req
        })
    }
    render() {
        return (<Main screenProps={{ navigate: this.props.navigation.navigate,navigation: this.props.navigation, show_menu: this.state.show_menu ,hideMenu:this.hideMenu}} />
        )}
}

export default MainNavigation;

