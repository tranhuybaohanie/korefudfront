import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    TouchableWithoutFeedback,
    Vibration,
    
} from 'react-native';
import VibrationReact from 'react-native-vibration'
import Sound from 'react-native-sound'
import { Actions, ActionConst } from 'react-native-router-flux';
import ls from 'react-native-local-storage'
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import FoodItemSection from './FoodItemSection';
const SIZE = 80;
class FoodItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Language: "en",
            itemsMenuComponent: this.props.itemsMenuComponent,
            sliderSession: 0,
            email:"",
            Cart: this.props.Cart[0].itemList
        }
    }

    btnAddCart = (itemID) => {
        Vibration.vibrate(50);
        let sound = new Sound('finish.mp3', Sound.MAIN_BUNDLE, (error) => {
            if (error) {} else {
                sound.setVolume(0.5);
                sound.play((success) => {
                    if (success) {
                    } else {
                        sound.reset();
                    }
                });
            }
        });

        var Cart=this.state.Cart;
        if (!Cart.some(e => e.id === itemID)){
        var itemCart = { id: itemID, quantity: 1 }
        Cart.push(itemCart)
        ls.save('Cart', Cart).then(() => { })
        this.props.UpdateCart([{
            itemList: Cart
        }])}
    }
    componentDidMount() {
        ls.get('email').then((email) => {this.setState({email})})

    }
    componentWillReceiveProps(newprops) {

        if (newprops.Cart[0].itemList) {
            this.setState({
                Cart: newprops.Cart[0].itemList
            })
        }
        // if (this.props.sliderSession != newprops.sliderSession){
            this.setState({ itemsMenuComponent: newprops.itemsMenuComponent, sliderSession: newprops.sliderSession })
        // }
      
        var previousLang = this.props.Lang[0]["lang"];
        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });

        }

    }

    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }

    stran = (key) => {
        return stranl(this.state.Language, key);
    }

    render() {

        var lang = this.state.Language;
        return (
         
            <View>
                {this.state.itemsMenuComponent.map((item, index) => {
                    return (
                        <FoodItemSection item={item} key={index} email={this.state.email} navigate={this.props.navigate}></FoodItemSection>
                    )
                })

                }

            </View>

        )
    }
}






const mapStateToProps = state => {
    return {
        Lang: state.Language,
        Cart: state.Cart,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        },
        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodItem);