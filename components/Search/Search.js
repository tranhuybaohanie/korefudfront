import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
  FlatList,
  Button,
  SearchBar,
  Icon,
  ScrollView,
  Dimensions
} from 'react-native';
import Svg, {
  Circle,
  Ellipse,
  G,
  LinearGradient,
  RadialGradient,
  Line,
  Path,
  Polygon,
  Polyline,
  Rect,
  Symbol,
  Text as TextSvg,
  Use,
  Defs,
  Stop
} from 'react-native-svg';
import { Actions, ActionConst } from 'react-native-router-flux';
import { navigate } from 'react-navigation'
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import FoodList from '../../Mock/FoodList';
import FoodItem from './FoodItem'
import Header from '../Shared/Header';
import { GetAllFood } from '../../Networking/FetchApi'
import FoodListMock from '../../Mock/FoodList'
import SquareItem from './SquareItem'
import { StackNavigator, TabNavigator, ThemeProvider, uiTheme, Toolbar, NavigationActions } from 'react-navigation'
import { db } from '../../utlils/firebaseConfig';
import Slider from './Slider';
import SliderLoader from './SliderLoader';
import CategoryItem from './CategoryItem';
import CategoryItemLoader from './CategoryItemLoader';
import 'react';

import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import SquareItemTrend from './SquareItemTrend';

const SIZE = 80;
class HomeFood extends Component {

  constructor(props) {
    super(props);

    this.state = {
      Language: this.props.Lang[0]["lang"],
      title: "Food",
      isLoading: false,
      foodListFromServer: [],
      itemsMenu: [],
      itemsMenuComponent: [],
      itemsMenuComponentSearch: [],
      itemsCategory: [],
      itemsCategoryComponent: [],
      itemsSlider: [],
      itemsSliderComponent: [],
      config_menu: {},
      sliderSession:0,
      searchKey:"",

      
    };
    
    this._onPress = this._onPress.bind(this);
    this.growAnimated = new Animated.Value(0);
  }

  componentWillReceiveProps(newprops) {
  
    if( newprops.screenProps.navigation){
      this.setState({searchKey: newprops.screenProps.navigation.getParam('searchKey')})
    }
    var previousLang = this.props.Lang[0]["lang"];
    var newLang = newprops.Lang[0]["lang"];
    if (this.state.Language != newLang) {
      this.setState({
        Language: newprops.Lang[0]["lang"],
      });

     
    }
  }
  componentWillMount() {

  }
  componentDidMount() {
   

    actionRedux.getMenuConfig((resultConfig) => {
      this.setState({
        itemsMenu: [],
        itemsMenuComponent: [],
        itemsCategory: [],
        itemsCategoryComponent: [],
        itemsSlider: [],
        itemsSliderComponent: [],
        config_menu: resultConfig
      })
      resultConfig.itemsMenu.map((item, id) => {
        if (!this.state.itemsMenu.includes(item)) {
          actionRedux.getProductByID(item, (result) => {
            var product = this.state.itemsMenuComponent;
            var itemMenu = this.state.itemsMenu;
            var itemsCategory = this.state.itemsCategory;
            var itemsCategoryComponent = this.state.itemsCategoryComponent;
            var itemsSlider = this.state.itemsSlider;
            var itemsSliderComponent = this.state.itemsSliderComponent;
           
           
           
              if (resultConfig.slider.includes(result.id)) {
                if (itemsSlider.includes(result.id)){
                  itemsSliderComponent[itemsSlider.indexOf(result.id)]=result;
                }else{
                itemsSlider.push(result.id)
                itemsSliderComponent.push(result)
                }
              } 
        
              if (itemMenu.includes(result.id)) {
                product[itemMenu.indexOf(result.id)] = result;
              } else {
              
                itemMenu.push(result.id)
              product.push(result)
              }
             
              // console.log(result)
              if (!itemsCategory.includes(result.product_category_id)) {
                itemsCategory.push(result.product_category_id)
                itemsCategoryComponent.push(result)

              }
              this.setState({
                itemsMenu: itemMenu,
                itemsMenuComponent: product,
                itemsCategory,
                itemsCategoryComponent,
                itemsSlider,
                itemsSliderComponent,
                sliderSession: this.state.sliderSession+1
              }, () => {
              
              })
            
          })
        }
      })
    })


  }
  refreshDataFromServer = () => {
    GetAllFood('hotel', 'GET', null).then((res) => {

    });

  }

  static navigationOptions = ({ navigation }) => {


    return {
      title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'find' : navigation.state.params.title,

      //  headerRight:<Button title="info"></Button>,
      header: { visible: true },
      //  headerTinColor:'red',
      //   activeTintColor:'blue',
      tabBarIcon: ({ focused }) => (

        <Image
          source={require('../Images/cart.png')}
          style={{
            width: 26, height: 26,
            tintColor: focused ? 'green' : 'gray',
          }}
        />

      )
    }
  }

  _onPress() {
    if (this.state.isLoading) return;

    this.setState({ isLoading: true });

    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 300,
      easing: Easing.quad,
    }).start();

    setTimeout(() => {
      Actions.pop();
    }, 500);
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  search=(searchKey)=>{

    // actionRedux.getMenuConfig((resultConfig) => {
    //   this.setState({
    //     itemsMenu: [],
    //     itemsMenuComponent: [],
    //     itemsCategory: [],
    //     itemsCategoryComponent: [],
    //     itemsSlider: [],
    //     itemsSliderComponent: [],
    //     config_menu: resultConfig
    //   })
    //   resultConfig.itemsMenu.map((item, id) => {
    //     if (!this.state.itemsMenu.includes(item)) {
    //       actionRedux.getProductByID(item, (result) => {
    //         var product = this.state.itemsMenuComponent;
    //         var itemMenu = this.state.itemsMenu;
    //         var itemsCategory = this.state.itemsCategory;
    //         var itemsCategoryComponent = this.state.itemsCategoryComponent;
    //         var itemsSlider = this.state.itemsSlider;
    //         var itemsSliderComponent = this.state.itemsSliderComponent;
           
    //        if(eval("result.name_"+this.state.Language)&&eval("result.name_"+this.state.Language+".toLowerCase().includes(searchKey.toLowerCase())")){
           
    //           if (resultConfig.slider.includes(result.id)) {
    //             if (itemsSlider.includes(result.id)){
    //               itemsSliderComponent[itemsSlider.indexOf(result.id)]=result;
    //             }else{
    //             itemsSlider.push(result.id)
    //             itemsSliderComponent.push(result)
    //             }
    //           } 
        
    //           if (itemMenu.includes(result.id)) {
    //             product[itemMenu.indexOf(result.id)] = result;
    //           } else {
              
    //             itemMenu.push(result.id)
    //           product.push(result)
    //           }
             
    //           // console.log(result)
    //           if (!itemsCategory.includes(result.product_category_id)) {
    //             itemsCategory.push(result.product_category_id)
    //             itemsCategoryComponent.push(result)

    //           }
    //           this.setState({
    //             itemsMenu: itemMenu,
    //             itemsMenuComponent: product,
    //             itemsCategory,
    //             itemsCategoryComponent,
    //             itemsSlider,
    //             itemsSliderComponent,
    //             sliderSession: this.state.sliderSession+1
    //           }, () => {
              
    //           })
    //         }
            
    //       })
    //     }
    //   })
    // })
    
    this.setState({searchKey,
      itemsMenuComponentSearch:this.state.itemsMenuComponent.filter(item=>(item.name_vi&&item.name_vi.toLowerCase().includes(searchKey.toLowerCase()))||(item.name_vi&&item.name_vi.toLowerCase().includes(searchKey.toLowerCase())))
      // .filter(x=>{
      //  x.name_vi?x.name_vi.includes("a"):true
      // })
    },()=>{
      // console.log("hi--------")
      // console.log(this.state.itemsMenuComponentSearch) 
    })
    
  }
  onScroll = (event) => {
    // var currentOffset = event.nativeEvent.contentOffset.y;
    // var direction = currentOffset > this.offset ? 'down' : 'up';
    // this.offset = currentOffset;
    // if (direction == "down") {
    //   this.props.screenProps.hideMenu(true)
    // } else {
    //   this.props.screenProps.hideMenu(false)
    // }
  }
  render() {
   
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, SIZE],
    });
    var { config_menu } = this.state;

    var lang = this.state.Language;
    var transl =this.stran;
    return (
<View> 
<View style={{zIndex:1000}}>
  <Header onSearchPage={true} searchKey={this.state.searchKey} setSearchKey={(searchKey)=>{this.search(searchKey)}}></Header>
  </View>
      <ScrollView style={styles.container} onScroll={this.onScroll}>
        <View style={styles.compoItem}>
          <Text style={styles.textTitle}>{this.state.searchKey!=""?this.state.itemsMenuComponent?this.state.itemsMenuComponentSearch.length+" result for "+this.state.searchKey:0 +transl("results")+ ": "+this.state.searchKey:"Menu for today(your searching not exceutes)"}</Text>
          <FoodItem navigate={this.props.screenProps.navigate} sliderSession={this.state.sliderSession} itemsMenuComponent={this.state.searchKey==""?this.state.itemsMenuComponent:this.state.itemsMenuComponentSearch}></FoodItem>
         
        </View>
      </ScrollView>
      </View>
    );
  }
}
const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    // flex: 1,

    backgroundColor: 'white'
    //alignItems: 'flex-end',
    //justifyContent: 'flex-end',
  },
  textTitle: {
    marginLeft: 8,
    marginTop: 10,
    marginBottom: 7,
    fontSize: 17,
    color:'pink',
    fontWeight: 'bold'


  },
  compoItem: {
    backgroundColor: 'white',
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 40

  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    borderRadius: 100,
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  circle: {
    height: SIZE,
    width: SIZE,
    marginTop: -SIZE,
    borderRadius: 100,
    backgroundColor: '#F035E0',
  },
  image: {
    width: 24,
    height: 24,
  },
});

const mapStateToProps = state => {
  return {
    Lang: state.Language,

  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeFood);