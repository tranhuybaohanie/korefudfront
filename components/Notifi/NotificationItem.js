import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';

import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import ls from 'react-native-local-storage'
import { sound } from './../../utlils/sound'
import Swipeout from 'react-native-swipeout'
const SIZE = 80;
class CartFoodItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Language: this.props.Lang[0]["lang"],
            item: this.props.item.item,

        }


    }

    componentDidMount() {


    }
    componentWillReceiveProps(newprops) {




        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });

        }

    }


    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }

    stran = (key) => {
        return stranl(this.state.Language, key);
    }
    showDetail = (item) => {
        // this.props.navigate("FoodIDetail", { item })
    }
    render() {

        var lang = this.state.Language;
        var stranl = this.stran;



        var item = this.state.item
        var finalTime="";
        var  create_date =item.create_date;
        var time =create_date.split(",")[0]
        var date =create_date.split(",")[1].split("/")
        var  d = new Date().toLocaleString("vi");
        var dt =d.split(",")[0]
        var dd=d.split(",")[1].split("/")
        var setNewRead=false;
        if(dd[2]==date[2]&&dd[1]==date[1]){
        if(dd[0]==date[0]){
        finalTime=this.state.Language=="en"?"Today at "+time:"Hôm nay lúc "+time
        setNewRead=time.split(":")[0]==dt.split(":")[0]&&dt.split(":")[1]-time.split(":")[1]<15?true:false
            
        }else{
        finalTime=dd[0]-date[0]+ this.state.Language=="en"?" days ago at "+time:" ngày trước lúc "+time
        }
        }else{
        finalTime=create_date
        }

        var swipeoutBtns = typeof this.props.pagefor == "string" && this.props.pagefor === "checkout" ? [] : [
            {
                onPress: () => {
                    this.deleteCart()
                },
                text: "Delete",
                type: "delete"

            }
        ];
        return (
            <View key={this.props.index} style={{ ...styles.bigcontainer, display: this.state.Deleled ? "none" : "flex", backgroundColor: "white" }}>
                <Swipeout style={[styles.bigcontainer, { borderColor: setNewRead?"#13fde9a3":"orange" }]} autoClose={true} onClose={(secid, rowid, direction) => { }} onOpen={(secid, rowid, direction) => {

                }}
                   

                >

                    <TouchableWithoutFeedback key={this.props.index}

                    >
                        <View key={1} style={styles.container}>

                            <View style={styles.content}>
                                <View style={{ flexDirection: "row" }}>
                                    <Image source={{ url: item.img_url }} style={{ height: 70, width: 70 }}></Image>
                                    <View style={styles.detailcontent}>
                                        <Text style={styles.namefood}>{this.limitString(eval("item.content_" + lang), 60)}</Text>
                                        <Text style={{fontSize:12,fontWeight:"100"}}>{finalTime}</Text>
                                        
                                    </View>
                                </View>
                               

                            
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                    {/* )
                })

                } */}


                </Swipeout>
            </View>

        )
    }
}



const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
    bigcontainer: {
        backgroundColor: "white",
        margin: 5,
        padding: 7,
        borderWidth:1,
        borderColor:"orange",
        borderRadius:20
    },
    container: {
        flex: 1,
        flexDirection: "column",
        //backgroundColor: 'white',

        marginTop: 3,

    },
    content: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        //  backgroundColor: 'none',

        // marginTop: 2,
        height: 70
    },
    detailcontent: {
        marginLeft: 3,
        height: 105
    },
    quantityBox: {
        minWidth: 20,
        flexDirection: "column",
        alignContent: "center",
        justifyContent: "space-between",
        marginLeft: 3,
        borderRadius: 3,
        borderColor: "black",
        borderWidth: 1,
        backgroundColor: "white",
        height: 57
    },
    detailcontentback: {
        position: 'absolute',
        left: 0,
        top: 100,
        width: Dimensions.get('window').width,
        height: 100,
        backgroundColor: "white",
    }, namefood: {
        fontSize: 15,
        width:WDevice-100,
        color: "black"
    }, price: {
        fontWeight: "100",
        marginTop: 5,
        fontSize: 13,
        color: "red",


    }, pricepromotion: {
        fontWeight: "100",
        fontSize: 11,

        color: "gray",
        textDecorationLine: "line-through",


    }, description: {
        color: "white"
    },
    readmore: {
        fontWeight: "bold"
    },
    comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center",
        backgroundColor: "white",
        marginTop: 10,
        borderRadius: 20,
    },
    comboitem: {
        borderRadius: 20,
        height: 40,
        flex: 0.33,
        marginTop: 3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})






const mapStateToProps = state => {
    return {
        Lang: state.Language,
        Cart: state.Cart,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {

        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(CartFoodItem);