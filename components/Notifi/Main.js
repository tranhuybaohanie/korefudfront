import React, { Component } from 'react';
import Notification from './Notification';
import { StackNavigator, TabNavigator } from 'react-navigation';
import Header from '../Shared/Header';

import {
    TabBarBottom,
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Button,
    SearchBar,
    Dimensions,
    Icon,
    ImageBackground,
    TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import * as actionRedux from '../../Storage/Actions/Index';

const WDevice = Dimensions.get('window').width;

// this main to fix header and contain submain
const Main = TabNavigator({


    MainHome: {
        screen: (props) => { return (<Notification screenProps={{ setNotificationNumber:props.screenProps.setNotificationNumber,navigate: props.screenProps.navigate,navigation: props.screenProps.navigation,hideMenu: props.screenProps.hideMenu }}></Notification>) },
        navigationOptions: {

            tabBarLabel: "Tab 1",

        }
    }
},
    {

        ...TabNavigator.Presets.AndroidTopTabs,

        tabBarComponent: (props, state) => {
            return (<View style={{ zIndex: 1000 }}>
                {/* {...TabNavigator.Presets.AndroidTopTabs} */}
                <Header screenProps={{ show_menu: props.screenProps.show_menu }}></Header>

            </View>)
        }


    })

class MainNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_menu: true,
            lengthNoti: 0,
           
        }
    }
    componentDidMount() {
        // this.setNotificationNumber(this.state.lengthNoti)
    }
    setNotificationNumber=(lengthNoti)=>{
        
        this.props.navigation.setParams({
            lengthNoti
        });
    }

    static navigationOptions = ({ navigation, state }) => {
        const { params = {} } = navigation.state;
        return {
            //     tabBar: (navigation, defaultOptions) => ({
            // ...defaultOptions,
            tabBarIcon: ({ tintColor, focused }) =>
            <TouchableWithoutFeedback onPress={()=>{
                
               actionRedux.setReadedNotification()
            navigation.navigate("Notifi")
            }}>
            <View style={{width:25,height:25}}>
             {params.lengthNoti>0?
             <View style={{ borderRadius: 10,zIndex:1000, position: "absolute",width:18, bottom: 13, left: 15,backgroundColor:"red", zIndex:3,   borderRadius: 10, padding: 2, }}><Text style={{textAlign: "center",color: "white",fontSize: 11 }}>{params.lengthNoti}</Text></View>:null}
 
            <Image
                source={focused ? require('./../Images/bellactive.png') : require('./../Images/bell.png')}
                style={{
                    width: 25, height: 25,
                    //tintColor: focused ? 'green' : 'gray',
                }}
            />
            </View>
            </TouchableWithoutFeedback>
  
            //         label: ({ tintColor, focused }) => (
            //             <Text style={{ color: tintColor }}>
            //                 Chat
            //   </Text >
            // )
        }
    }
    hideMenu = (req) => {
        this.setState({
            show_menu: !req
        })
    }
    render() {
        return (<Main screenProps={{ navigate: this.props.navigation.navigate,navigation: this.props.navigation,setNotificationNumber:this.setNotificationNumber, show_menu: this.state.show_menu, hideMenu: this.hideMenu }} />
        )
    }
}
const mapStateToProps = state => {
    return {
        Lang: state.Language,
        Cart: state.Cart,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        },
        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainNavigation);


