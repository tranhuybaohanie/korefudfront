import React, { Component } from 'react';
import Cart from './Cart';
import { StackNavigator, TabNavigator } from 'react-navigation';
import Header from '../Shared/Header';
import Order from './Order';
import {
    TabBarBottom,
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Button,
    SearchBar,
    Dimensions,
    Icon,
    ImageBackground
} from 'react-native';
import { connect } from 'react-redux';

const WDevice = Dimensions.get('window').width;

const SubMain = TabNavigator({

    Cart: {
        screen: Cart,
        // navigationOptions: {

        //     tabBarLabel: "Food",

        // }
    },

    Trackmyorder: {
        screen: Order,
        // navigationOptions: {

        //     tabBarLabel: "Drink",

        // }
    },
 


},
    {
        // lazy: false,
        // tabBarPosition: 'Bottom',
        swipeEnabled: false,

        ...TabNavigator.Presets.AndroidTopTabs,


        tabBarOptions: {
            // scrollEnabled: true,
            showIcon: false,
            showLabel: true,



            activeTintColor: '#ff0000',
            activeBackgroundColor: 'rgb(29, 144, 175)',
            animationEnabled: true,
            labelStyle: {
                color: 'black',
                fontSize: 12,

                fontWeight: 'bold',
                width: Dimensions / 3
            },

            tabStyle: {
                width: (WDevice / 2) - 5,

                //borderLeftWidth: 1,
                //borderRightWidth: 1,

            },

            style: {
                // width: 600,
                backgroundColor: 'white',
                // color:'black'
            },
        }
    })




// this main to fix header and contain submain
const Main = TabNavigator({


    MainHome: {
        screen: (props) => { return (<SubMain screenProps={{ navigate: props.screenProps.navigate,navigation: props.screenProps.navigation,hideMenu: props.screenProps.hideMenu }}></SubMain>) },
        navigationOptions: {

            tabBarLabel: "Tab 1",

        }
    }
},
    {

        ...TabNavigator.Presets.AndroidTopTabs,

        tabBarComponent: (props, state) => {
            return (<View style={{ zIndex: 1000 }}>
                {/* {...TabNavigator.Presets.AndroidTopTabs} */}
                <Header screenProps={{ show_menu: props.screenProps.show_menu }}></Header>

            </View>)
        }


    })

class MainNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_menu: true,
            lengthCart: 0,
            Cart: this.props.Cart[0].itemList
        }
    }
    componentDidMount() {

        this.props.navigation.setParams({
            lengthCart: this.state.Cart.length
        });
    }
    componentWillReceiveProps(newprops) {
        
        if (this.state.lengthCart != newprops.Cart[0].itemList.length) {
            this.props.navigation.setParams({
                lengthCart: newprops.Cart[0].itemList.length
            });
            this.setState({ lengthCart: newprops.Cart[0].itemList.length })
        }
    }
    static navigationOptions = ({ navigation, state }) => {
        const { params = {} } = navigation.state;
        return {
            //     tabBar: (navigation, defaultOptions) => ({
            // ...defaultOptions,
            tabBarIcon: ({ tintColor, focused }) =>
            <View style={{width:25,height:25}}>
             {params.lengthCart>0?
             <View style={{ borderRadius: 10,zIndex:1000, position: "absolute",width:18, bottom: 13, left: 15,backgroundColor:"black", zIndex:3,   borderRadius: 10, padding: 2, }}><Text style={{textAlign: "center",color: "white",fontSize: 11 }}>{params.lengthCart}</Text></View>:null}
                      <Image
                source={focused ? require('./../Images/cartactive.png') : require('./../Images/cart.png')}
                style={{
                    width: 24, height: 24,
                    tintColor: focused ? 'red' : '#3f4040',
                }}
            />
            </View>
  
            //         label: ({ tintColor, focused }) => (
            //             <Text style={{ color: tintColor }}>
            //                 Chat
            //   </Text >
            // )
        }
    }
    hideMenu = (req) => {
        this.setState({
            show_menu: !req
        })
    }
    render() {
        return (<Main screenProps={{ navigate: this.props.navigation.navigate,navigation: this.props.navigation, show_menu: this.state.show_menu, hideMenu: this.hideMenu }} />
        )
    }
}
const mapStateToProps = state => {
    return {
        Lang: state.Language,
        Cart: state.Cart,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        },
        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainNavigation);


