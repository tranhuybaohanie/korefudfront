import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';

import { Actions, ActionConst } from 'react-native-router-flux';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';


const SIZE = 80;
class FoodItem extends Component {
 constructor(props) {
    super(props);
    
    this.state = {
           Language:"en",
        itemsMenu:[],
        itemsMenuComponent:[],
        itemsCategory:[],
        itemsCategoryComponent: []
    }
 }


    componentWillReceiveProps(newProps) {
          if (this.props != newProps) {
            this.setState({
                Language: newProps.Lang[0]["lang"],
            });

        }


                      this.setState({
                          itemsMenu: newProps.itemsMenu,
                          itemsCategory: newProps.itemsCategory,
                          itemsCategoryComponent: newProps.itemsCategoryComponent,
                      })

  }

limitString =(string,num)=>{
    
if(typeof string=="string")
return string.length>num ? string.slice(0,num)+"...":string.slice(0,num);
}


   stran = (key) => {
        return stranl(this.state.Language, key);
    }
    showDetail = (item_id,itemsMenu,product_category_name) => {
        this.props.navigate("CategoryPage", { item_id, itemsMenu, product_category_name})
    }
    render() {

        var lang = this.state.Language;


        return (
            <View >
                {this.state.itemsCategoryComponent.map((item,id)=>{
              return (
                  <TouchableWithoutFeedback onPress={() => { this.showDetail(item.product_category_id, this.state.itemsMenu, eval("item.product_category_name_" + lang)) }} >
                  <View style={styles.container}>
                      <Image style={{ flex: 1, height: 200, borderRadius: 10 }} source={{ url: item.product_category_img_url }} />

                      <View style={styles.detailcontentback}></View>
                      <Image style={{ flex: 1, position: 'absolute', top: 45, left: 8, height: 60, width: 60, borderRadius: 10 }} source={{ url: item.product_category_img_url  }} />
                      <View style={styles.detailcontent}>
                      <Text style={styles.detailcontentTextTitle}>{eval("item.product_category_name_"+lang)}</Text>
                      <Text style={styles.detailcontentTextDescription}>{eval("item.product_category_description_"+lang)}</Text>
                      </View>

                  </View>
                  </TouchableWithoutFeedback>
              )

          })
           
        }      
            </View>  
        )
    }
}


const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: 'white',
         height: 110, 
         width: WDevice-16, 
         marginLeft: 8,
        marginRight: 4,
         borderRadius:10,
         marginBottom:8
        //backgroundColor: 'white',

        

    },
    content: {
        flex: 1,
        flexDirection: "column",
        marginTop: 3,
        height: 200
    },
    detailcontent: {
        marginLeft:4,
        position: 'absolute',
        left: 80,
        top: 60,
        fontWeight:'bold',
       // backgroundColor: "#e0cdcd00",
        height: 105,
        color:'white',
    },
     detailcontentTextTitle: {
       
        fontWeight:'bold',
        fontSize:18,
        color:'white',
    },
     detailcontentTextDescription: {
       
        fontWeight:'bold',
        fontSize:14,
        color:'white',
    },
    detailcontentback: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: Dimensions.get('window').width-16,
        height: 110,
        backgroundColor: "#79747487",
        borderRadius: 10
    },namefood:{
        fontSize:25,
        fontWeight:"bold",
        color:"white"
    },price:{
        marginLeft: 3,
        position: 'absolute',
        left: 0,
        top: 75,
        // backgroundColor: "#e0cdcd00",
        height: 105,
        fontWeight:"bold",
        color:"yellow",
        

    },pricepromotion:{
        marginLeft: 3,
        position: 'absolute',
        left: 0,
        top: 89,
        // backgroundColor: "#e0cdcd00",
        height: 105,
        fontWeight: "bold",
        color: "red",
        textDecorationLine: 'line-through'

    },description:{
        color:"white"
    },
     readmore:{
        fontWeight:"bold"
    },
    comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center"
    },
    comboitem: {
        height: 40,
        flex: 0.33,
        marginTop:3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})




const mapStateToProps = state => {
    return {
        Lang: state.Language,

    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodItem);
