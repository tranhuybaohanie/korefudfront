import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';

import { Actions, ActionConst } from 'react-native-router-flux';

import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import ls from 'react-native-local-storage'
import { sound } from './../../utlils/sound'
import Swipeout from 'react-native-swipeout'
const SIZE = 80;
class OrderFoodItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Language: this.props.Lang[0]["lang"],
            sliderSession: 0,
            item: this.props.item.item,
            // quantity: this.props.item.item.item,
            Cart: this.props.Cart[0].itemList,
            Deleled: false
        }
       


    }
    btnSetQuantity = (sign) => {
        var Cart = this.state.Cart;
        var { item } = this.state;
        if (Cart.some(e => e.id === item.id)) {
            sound("click.mp3", 0.1)
            var math = sign == "+" ? +1 : Cart[Cart.findIndex(i => i.id === item.id)].quantity > 1 ? -1 : 0;
            Cart[Cart.findIndex(i => i.id === item.id)].quantity = Cart[Cart.findIndex(i => i.id === item.id)].quantity + math;
            ls.save('Cart', Cart).then(() => { })
            this.props.UpdateCart([{
                itemList: Cart
            }])

        }
    }
    componentDidMount() {

    }
    componentWillReceiveProps(newprops) {

        this.setState({ item: newprops.item.item })



        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });

        }

    }
    deleteCart = () => {

        var Cart = this.state.Cart;
        var { item } = this.state;
        // alert(item.id)
        if (Cart.some(e => e.id === item.id)) {
            Cart = Cart.filter(i => i.id != item.id)
            sound("click.mp3", 0.1)
            ls.save('Cart', Cart).then(() => { })
            this.props.UpdateCart([{
                itemList: Cart
            }])
            // this.setState({ Deleled: true })
            // this.props.refresh(this.props.index)
        }
    }

    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }

    stran = (key) => {
        return stranl(this.state.Language, key);
    }
    showDetail = (item) => {
        // this.props.navigate("FoodIDetail", { item })
    }

    render() {

        var lang = this.state.Language;
        var stranl = this.stran;
        var item = this.state.item
        var today = new Date();
        var create_date = new Date(item.create_date);
        var orderTime = item.create_date
        // if (today.getDate() == create_date.getDate() && today.getMonth() == create_date.getMonth() && today.getFullYear() == create_date.getFullYear()) {
        //     orderTime = stranl("Today ") + create_date.getHours() + ":" + create_date.getMinutes()
        // } else {
        //     orderTime = create_date.getDate() + "/" + create_date.getMonth() + "/" + create_date.getFullYear() + " " + create_date.getHours() + ":" + create_date.getMinutes()
        // }



        var swipeoutBtns = typeof this.props.pagefor == "string" && this.props.pagefor === "checkout" ? [] : [
            {
                onPress: () => {
                    this.deleteCart()
                },
                text: "Delete",
                type: "delete"

            }
        ];
      
        return (
            <View key={this.props.index} style={[styles.bigcontainer, { display: this.state.Deleled ? "none" : "flex", backgroundColor: "white" }]}>

                {item.cancel_state == true ?
                    <View key={1} style={styles.container}>

                        <View style={styles.content}>
                            <View style={{ flexDirection: "row" }}>
                                <View style={{ backgroundColor: "pink" }}>
                                    <Text style={[styles.namefood, { color: "red" }]}>#{this.limitString(item.id, 35)}</Text>
                                    <Text style={{ fontSize: 12, fontWeight: "100" }}>({stranl("Total: ") + item.totalItem + "items, " + item.totalMoney + " VND" + "-" + orderTime}</Text>
                                    <Text>{stranl("Reason for cancel: ") + item.content_cancel + "\n Log: " + eval("item.log_" + this.state.Language)}</Text>
                                </View>
                                
                            </View>
                            <View >
                                    <Text style={{ textAlign: "center", color: "white", backgroundColor: "green" }} onPress={() => this.props.showDetail(item)}>{stranl("Detail")}</Text>

                                </View>
                        </View>
                    </View>
                    :

                    <TouchableWithoutFeedback key={this.props.index}

                    // onPress={() => { this.showDetail(item) }} 
                    >
                        <View key={1} style={styles.container}>

                            <View style={styles.content}>
                                <View style={{ flexDirection: "row" }}>
                                    {/* <Image source={{ url: item.img_url }} style={{ height: 70, width: 70 }}></Image> */}
                                    <View style={styles.detailcontent}>
                                        <Text style={[styles.namefood, { color: "blue" }]}>#{this.limitString(item.id, 35)}</Text>
                                        <Text style={{ fontSize: 12, fontWeight: "100" }}>({stranl("Total: ") + item.totalItem + "items, " + item.totalMoney + " VND" + "-" + orderTime}</Text>
                                       {item.received_state? <Text style={{ fontSize: 12, fontWeight: "100" }}>({stranl("Acceptance: ") + item.totalItemAcceptance + "items, " + item.totalMoneyAcceptance + " VND" + "-" + item.AcceptanceTime}</Text>:null}
                                        <View style={{ flexDirection: "row", justifyContent: "space-between" ,width:WDevice-100}}>
                                            <Image source={item.received_state ? require("./../Images/accept.png") : require("./../Images/accept_disable.png")} style={{ width: 50, height: 50 }}></Image>
                                            <Image source={item.received_state ? require("./../Images/cooking.png") : require("./../Images/cooking_disable.png")} style={{ width: 50, height: 50 }}></Image>
                                            <Image source={item.finished_state ? require("./../Images/deliver.png") : require("./../Images/deliver_disable.png")} style={{ width: 50, height: 50 }}></Image>
                                            <Image source={item.delivered_state ? require("./../Images/received.png") : require("./../Images/received_disable.png")} style={{ width: 50, height: 50 }}></Image>
                                        </View>
                                    </View>
                                </View>
                                {/* <Text style={styles.detailcontentback}> </Text> */}

                                <View >
                                    <Text style={{ textAlign: "center", color: "white", backgroundColor: "green" }} onPress={() => this.props.showDetail(item)}>{stranl("Detail")}</Text>
                                    {!item.received_state ? <TouchableOpacity onPress={() => this.props.showCancel(item.id)}>
                                        <Text style={{ textAlign: "center", color: "#ffc107", marginTop: 15 }}>{stranl("Cancel")}</Text>
                                    </TouchableOpacity> :
                                        <Text style={{ textAlign: "center", color: "gray", marginTop: 15 }}>{stranl("Cancel")}</Text>}
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                }


            </View>

        )
    }
}


const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
    bigcontainer: {
        backgroundColor: "white",
        marginTop: 3,
        padding: 7
    },
    container: {
        
        flexDirection: "column",
        backgroundColor: "#fbf3e7fc",
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#ffc107',
        marginTop: 3,
        padding: 3,

    },
    content: {

        flexDirection: "row",
        justifyContent: "space-between",

    },
    detailcontent: {
        marginLeft: 3,

    },
    quantityBox: {
        minWidth: 20,
        flexDirection: "column",
        alignContent: "center",
        justifyContent: "space-between",
        marginLeft: 3,
        borderRadius: 3,
        borderColor: "black",
        borderWidth: 1,
        backgroundColor: "white",
        height: 57
    },
    detailcontentback: {
        position: 'absolute',
        left: 0,
        top: 100,
        width: Dimensions.get('window').width,
        height: 100,
        backgroundColor: "white",
    }, namefood: {
        fontSize: 12,

        color: "black"
    }, price: {
        fontWeight: "100",
        marginTop: 5,
        fontSize: 13,
        color: "red",


    }, pricepromotion: {
        fontWeight: "100",
        fontSize: 11,

        color: "gray",
        textDecorationLine: "line-through",


    }, description: {
        color: "white"
    },
    readmore: {
        fontWeight: "bold"
    },
    comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center",
        backgroundColor: "white",
        marginTop: 10,
        borderRadius: 20,
    },
    comboitem: {
        borderRadius: 20,
        height: 40,
        flex: 0.33,
        marginTop: 3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})






const mapStateToProps = state => {
    return {
        Lang: state.Language,
        Cart: state.Cart,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {

        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(OrderFoodItem);