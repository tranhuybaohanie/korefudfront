import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
  FlatList,
  Button,
  SearchBar,
  Icon,
  ScrollView,
  Dimensions,
  Alert,
  ImageBackground
} from 'react-native';
import Svg, {
  Circle,
  Ellipse,
  G,
  LinearGradient,
  RadialGradient,
  Line,
  Path,
  Polygon,
  Polyline,
  Rect,
  Symbol,
  Text as TextSvg,
  Use,
  Defs,
  Stop
} from 'react-native-svg';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { Actions, ActionConst } from 'react-native-router-flux';
import 'react';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import ls from 'react-native-local-storage'
import CartFoodItem from './CartFoodItem'
import { sound } from './../../utlils/sound'
import { connect } from 'react-redux';
import OrderFoodItem from './OrderFoodItem'

const SIZE = 80;
class Order extends Component {

  constructor(props) {
    super(props);

    this.state = {
      Language: this.props.Lang[0]["lang"],
      sliderSession: 0,
      OrderList: [],
      cancelForm: new Animated.Value(450),
      cancelID: "Loading..",
      detailForm: new Animated.Value(450),
      detailItem: {},
      reasonCancel: [],
      cancel_content: ""

    };

    this._onPress = this._onPress.bind(this);
    this.growAnimated = new Animated.Value(0);
  }

  componentWillReceiveProps(newprops) {



    var newLang = newprops.Lang[0]["lang"];
    if (this.state.Language != newLang) {
      this.setState({
        Language: newprops.Lang[0]["lang"],
      });

      const { navigation: { setParams } } = this.props;
      setParams({
        title: this.props.Lang[0]["lang"] == "en" ? "Order" : "Đơn hàng"
      });
    }
  }
  componentWillMount() {
    const { navigation: { setParams } } = this.props;
    setParams({
      title: this.props.Lang[0]["lang"] == "en" ? "Order" : "Đơn hàng"
    });
  }
  componentDidMount() {
    actionRedux.getOrder(result => {
      this.setState({
        OrderList: result
      })

    })
    actionRedux.getReasonCancel(result => {
      this.setState({
        reasonCancel: result
      })

    })
  }



  static navigationOptions = ({ navigation }) => {


    return {
      title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'find' : navigation.state.params.title,

      //  headerRight:<Button title="info"></Button>,
      header: { visible: true },
      swipeEnabled: false,
      //  headerTinColor:'red',
      //   activeTintColor:'blue',
      tabBarIcon: ({ focused }) => (

        <Image
          source={require('../Images/cart.png')}
          style={{
            width: 26, height: 26,
            tintColor: focused ? 'green' : 'gray',
          }}
        />

      )
    }
  }

  showCancel = (cancelID) => {
    this.setState({ cancelID })
    Animated.timing(this.state.cancelForm, {
      toValue: 10,
      duration: 300,
      easing: Easing.quad,
    }).start();
  }
  btnCloseCancelForm = () => {
    this.setState({ cancelID: "" })
    Animated.timing(this.state.cancelForm, {
      toValue: 450,
      duration: 300,
      easing: Easing.quad,
    }).start();
  }
  showDetail = (detailItem) => {
    this.setState({ detailItem})
    Animated.timing(this.state.detailForm, {
      toValue: 10,
      duration: 300,
      easing: Easing.quad,
    }).start();
  }
  btnCloseDetailForm = () => {
    this.setState({ detailItem: {} })
    Animated.timing(this.state.detailForm, {
      toValue: 450,
      duration: 300,
      easing: Easing.quad,
    }).start();
  }
  btnCancelOrder = () => {
    actionRedux.cancelOrder(this.state.cancelID, this.state.cancel_content, result => {

      if (!result) {
        var title = "";
        var content = "";

        title = this.stran("Warning");
        content = this.stran("Sorry, you cannot cancel order at this time.");
        this.btnCloseCancelForm()
        Alert.alert(
          title, content,
          [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          ],
          { cancelable: false }
        )
      } else {
        this.btnCloseCancelForm()
      }
    })
  }

  _onPress() {
    if (this.state.isLoading) return;

    this.setState({ isLoading: true });

    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 300,
      easing: Easing.quad,
    }).start();

    setTimeout(() => {
      Actions.pop();
    }, 500);
  }

  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  onScroll = (event) => {
    // var currentOffset = event.nativeEvent.contentOffset.y;
    // var direction = currentOffset > this.offset ? 'down' : 'up';
    // this.offset = currentOffset;
    // if (direction == "down") {
    //   this.props.screenProps.hideMenu(true)
    // } else {
    //   this.props.screenProps.hideMenu(false)
    // }
  }
  navigateCheckOut = () => {
    if (this.state.OrderList.length == 0) {
      Alert.alert(
        this.stran('Block Check Out'),
        this.stran('Go placing items to cart to check out.'),
        [

          { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'OK', onPress: () => this.props.screenProps.navigate("Home") },
        ],
        { cancelable: false }
      )
    } else {
      this.props.screenProps.navigate("CheckOut")
    }

  }
  limitString = (string, num) => {

    if (typeof string == "string")
        return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
}

  render() {




    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, SIZE],
    });
    var lang = this.state.Language;
    var transl = this.stran;

    var item =this.state.detailItem;
    var radio_props = [];
    this.state.reasonCancel.map((item, id) => {

      radio_props.push({ label: eval("item.reason_" + this.state.Language), value: eval("item.reason_" + this.state.Language) })
    })

    return (
      <View style={styles.container} >
        <ScrollView onScroll={this.onScroll}>
          {this.state.OrderList.length == 0 ? <View>
            <ImageBackground source={require("./../Images/cart-empty-banner.jpg")} style={{ width: WDevice, height: 300, marginTop: 10, justifyContent: "center", alignItems: "center" }}>
              <TouchableOpacity onPress={() => this.props.screenProps.navigate("Home")}>
                <Svg
                  height="180"
                  width="180"
                >
                   {/* <Text style={{ fontSize: 18, color: 'white', textAlign: "center", paddingTop: 60 }}>{transl("Go placing your dishes to cart now ")}</Text> */}
                   <Defs>
                    <LinearGradient id="grad" x1="0" y1="0" x2="180" y2="0">
                      <Stop offset="0" stopColor="#ffc3a0" stopOpacity="0" />
                      <Stop offset="1" stopColor="#E94B3C" stopOpacity="0" />
                    </LinearGradient>
                  </Defs>

                  <Polygon
                    points="30,0 150,0 180,30 180,150 150,180 30,180 0,150 0,30 "
                    fill="url(#grad)"

                  />

                </Svg>

              </TouchableOpacity>
            </ImageBackground>
          </View> : null}
          <FlatList
            data={this.state.OrderList}
            extraData={this.props}
            // refreshing={this.state.refresh}
            // onRefresh={this.refresh}
            keyExtractor={item => item.id}
            renderItem={(item, rowID) => <OrderFoodItem item={item} index={rowID} showCancel={this.showCancel} showDetail={this.showDetail} />}
          />



        </ScrollView>
        <Animated.View style={[styles.cancelFormBack, { marginLeft: this.state.cancelForm }]}>

        </Animated.View>
        <Animated.View style={[styles.cancelForm, { left: this.state.cancelForm }]}>
          <Text style={{ fontSize: 15, color: "red", marginTop: 20, textAlign: "center" }}>{transl("Give us reason that you wanna cancel order: ") + this.state.cancelID}</Text>


          <RadioForm
            radio_props={radio_props}
            initial={false}
            animation={true}
            style={{ justifyContent: "flex-start" }}
            onPress={(value) => { this.setState({ cancel_content: value }) }}
          />
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            <TouchableOpacity onPress={this.btnCancelOrder}>
              <Text style={{ backgroundColor: "yellow", color: "red", padding: 10, borderRadius: 20, marginRight: 20 }}>{transl("Cancel order")}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.btnCloseCancelForm}>
              <Text style={{ backgroundColor: "green", color: "white", padding: 10, borderRadius: 20 }}>{transl("No, I wanna order")}</Text>
            </TouchableOpacity>
          </View>

        </Animated.View>

         <Animated.View style={[styles.cancelFormBack, { marginLeft: this.state.detailForm }]}>

        </Animated.View>

        <Animated.View style={[styles.detailForm, { left: this.state.detailForm }]}>
        <TouchableOpacity onPress={this.btnCloseDetailForm}>
              <Text style={{ backgroundColor: "green",textAlign:"center", color: "white", padding: 10, borderRadius: 20 }}>{transl("OK")}</Text>
       </TouchableOpacity>
          <View style={styles.detailcontent}>
            <Text style={[styles.namefood, { color: "blue" }]}>#{this.limitString(item.id, 35)}</Text>
            <Text style={{ fontSize: 12, fontWeight: "100" }}>({transl("Total: ") + item.totalItem + "items, " + item.totalMoney + " VND" + "-" + item.create_date}</Text>
            {item.received_state? <Text style={{ fontSize: 12, fontWeight: "100" }}>({transl("Acceptance: ") + item.totalItemAcceptance + "items, " + item.totalMoneyAcceptance + " VND" + "-" + item.AcceptanceTime}</Text>:null}
            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
              <Image source={item.received_state ? require("./../Images/accept.png") : require("./../Images/accept_disable.png")} style={{ width: 50, height: 50 }}></Image>
              <Image source={item.received_state ? require("./../Images/cooking.png") : require("./../Images/cooking_disable.png")} style={{ width: 50, height: 50 }}></Image>
              <Image source={item.finish_state ? require("./../Images/deliver.png") : require("./../Images/deliver_disable.png")} style={{ width: 50, height: 50 }}></Image>
              <Image source={item.delivered_state ? require("./../Images/received.png") : require("./../Images/received_disable.png")} style={{ width: 50, height: 50 }}></Image>
            </View>
          </View>
          <ScrollView>
          {item.received_state? <ScrollView>
                    <Text style={styles.container_top_profile}>{transl("Acceptance package:")}</Text>
                    <FlatList
                        data={item.Acceptance}
                        extraData={this.props}
                        // refreshing={this.state.refresh}
                        // onRefresh={this.refresh}
                        keyExtractor={item => item.id}
                        renderItem={(item, rowID) => <CartFoodItem item={item} index={rowID} deleteCart={this.deleteCart} pagefor={"checkout"} />}
                    />
          </ScrollView>:
          <ScrollView>
                    <Text style={styles.container_top_profile}>{transl("Package:")}</Text>
                    <FlatList
                        data={item.Cart}
                        extraData={this.props}
                        // refreshing={this.state.refresh}
                        // onRefresh={this.refresh}
                        keyExtractor={item => item.id}
                        renderItem={(item, rowID) => <CartFoodItem item={item} index={rowID} deleteCart={this.deleteCart} pagefor={"checkout"} />}
                    />
          </ScrollView>}
          </ScrollView>

        </Animated.View>

      </View>
    );
  }
}

const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: 'white',

    //alignItems: 'flex-end',
    // justifyContent: 'space-between',
  }, cancelForm: {
    padding: 20,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#ffc107',
    backgroundColor: "white",
    justifyContent: "center",
    width: WDevice - 20,

    top: 50,
    position: "absolute",

  },
  detailForm: {
    padding: 20,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#ffc107',
    backgroundColor: "white",
    justifyContent: "center",
    width: WDevice - 20,

    top: 10,
    position: "absolute",

  },
  cancelFormBack: {
    backgroundColor: "#dee2e69e",
    height: 800,
    width: WDevice,

    position: "absolute",

  },
  btnOrder: {
    backgroundColor: "white",
    width: WDevice,
    height: 50,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  textTitle: {
    marginLeft: 8,
    marginTop: 10,
    marginBottom: 7,
    fontSize: 17,
    fontWeight: 'bold'

  },
  compoItem: {
    backgroundColor: 'white',
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 40

  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    borderRadius: 100,
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  circle: {
    height: SIZE,
    width: SIZE,
    marginTop: -SIZE,
    borderRadius: 100,
    backgroundColor: '#F035E0',
  },
  image: {
    width: 24,
    height: 24,
  },
});


const mapStateToProps = state => {
  return {
    Lang: state.Language,
    Cart: state.Cart,
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {

    UpdateCart: (result) => {

      dispatch(actionRedux.actUpdateCart(result));

    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Order);